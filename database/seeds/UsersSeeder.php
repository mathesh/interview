<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [ 
            [ 
              'name' => 'admin',
              'email' => 'admin@admin.com',
              'password' => 'Test@123',
            ]];
            foreach($users as $user)
            {
                User::create([
                 'name' => $user['name'],
                 'email' => $user['email'],
                 'password' => Hash::make($user['password'])
               ]);
             }    
    }
}
