<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/crud', function () {
    return view('crud');
});

Route::get('/users','HomeController@getUsers')->name('user.all');

Route::post('/save','HomeController@save')->name('user.save');

Route::get('/export','HomeController@export')->name('user.export');


Route::get('/user','HomeController@getUser')->name('user.get');

Route::post('/update','HomeController@update')->name('user.update');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::group(['prefix'=>'company'],function()
{ 
    Route::get('delete/{id}','CompanyController@destroy')->name('company.delete');
    Route::post('update','CompanyController@update')->name('company.update');
    Route::get('import-data', 'ImportExcel\ImportExcelController@indexCompany')->name('company.import');
    Route::post('upload', 'ImportExcel\ImportExcelController@importCompany')->name('company.upload');
});

Route::group(['prefix'=>'employee'],function()
{ 
    Route::get('delete/{id}','EmployeeController@destroy')->name('employee.delete');
    Route::post('update','EmployeeController@update')->name('employee.update');
    Route::get('import', 'ImportExcel\ImportExcelController@indexEmployee')->name('employee.import');
    Route::post('upload-data', 'ImportExcel\ImportExcelController@importEmployee')->name('employee.upload');
});


Route::resource('company', 'CompanyController');
Route::resource('employee', 'EmployeeController');

