@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Employee List
                <a href="{{url('employee/create')}}" style="margin-left:60%;"> + Create Employee</a>/
                <a href="{{route('employee.import')}}"> Bulk upload</a></div>
                <div class="card-body">
                                   

<!-- Tabs content -->
        <table id="myTable" class="table table-striped table-bordered" style="width:100%">
            <thead>
               <tr>
                  <th>#S.No</th>
                  <th>Firstname</th>
                  <th>Lastname</th>
                  <th>Email </th>
                  <th>Phone</th>
                  <th>Designation</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
              <tbody id="data">
              @foreach($employees as $employee)
            <tr>
                <td>#{{$employee->id}}</td>
                <td>{{$employee->firstname}}</td>
                <td>{{$employee->lastname}}</td>
                <td>{{$employee->emp_email}}</td>
                <td>{{$employee->phone}}</td>
                <td>{{$employee->designation}}</td>
                <td>{{ucfirst($employee->status)}}</td>
                <td> <a  href="{{route('employee.edit', $employee->uuid)}}">Edit</a>
                  / <a href="{{url('employee/delete',$employee->uuid)}}">Delete</a></td>
            </tr>
  @endforeach     

              </tbody>
         </table>
<!-- Tabs content -->
{{ $employees->links() }}

                </div>
            </div>
        </div>
    </div>
</div>
<script>

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#logo').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this);
});

</script>
@endsection
