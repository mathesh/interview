@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Employee
                <a href="{{url('employee')}}" style="margin-left:60%;"> Employee List</a></div>
                <div class="card-body">
        <form class="form-horizontal" method="post" action="{{url('employee/update')}}" id="employee-form">
            <h3 style="text-align:center;">Enter Employee Details </h3>
              <div class="col-md-10" style="margin-top:20px;">

              <div class="form-group">
                    <label class="control-label col-sm-4" for="fname">Select Company:</label>
                    <div class="col-sm-8">
                        <select class="form-control" id="company" name="company_id" required>
                        @foreach($companies as $company)
                           <option  value="{{$company->id}}">{{$company->cname}}</option>
                        @endforeach   
                       </select>
                       <script>
                       $('#company').val("{{$employee->company_id}}");
                       </script>
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="fname">First Name:</label>
                    <div class="col-sm-8">
                       <input type="text" class="form-control" id="fname" placeholder="Enter First Name" required name="firstname" value="{{old('firstname',$employee->firstname)}}">
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="fname">Last Name:</label>
                    <div class="col-sm-8">
                       <input type="text" class="form-control" id="lname" placeholder="Enter Last Name" required name="lastname" value="{{old('lastname',$employee->lastname)}}">
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Email:</label>
                    <div class="col-sm-8">
                       <input type="email" class="form-control" id="email" placeholder="Email" required name="emp_email" value="{{old('emp_email',$employee->emp_email)}}">
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="logo">Phone :</label>
                    <div class="col-sm-8">
                       <input type="number"  class="form-control"   required name="phone" value="{{old('phone',$employee->phone)}}">
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="logo">Designation :</label>
                    <div class="col-sm-8">
                       <input type="text" class="form-control"   required name="designation" value="{{old('designation',$employee->designation)}}">
                    </div>
                 </div>
              </div>
              <div class="col-md-10">
                 
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Status:</label>
                    <div class="col-sm-8">
                    <input type="radio" name="status" id="active" @if($employee->status=='active') checked @endif value="active">Active
                     <input type="radio" name="status"  id="inactive"  @if($employee->status=='inactive') checked @endif value="inactive">Inactive
                    </div>
                 </div>
              </div>
            <div class="row">
               <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-8">
                     <input type="hidden" name="id" value="{{$employee->uuid}}">
                     <button type="submit" class="btn btn-primary">Update</button>
                  </div>
               </div>
               @csrf
             </div>
           </form>
<!-- Tabs content -->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
         
         function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#logo').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this);
});
        
         
         $(function () { //import-form
         $('#company-form').parsley().on('field:validated', function() {
         var ok = $('.parsley-error').length === 0;
         $('.bs-callout-info').toggleClass('hidden', !ok);
         $('.bs-callout-warning').toggleClass('hidden', ok);
         })
         .on('form:submit', function() {
             return true;
         });
       
         });
      </script>
@endsection
