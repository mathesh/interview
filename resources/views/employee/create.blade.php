@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Employee
                <a href="{{url('employee')}}" style="margin-left:60%;"> Employee List</a></div>
                <div class="card-body">
        <form class="form-horizontal" method="post" action="{{url('employee')}}" id="employee-form" enctype="multipart/form-data">
            <h3 style="text-align:center;">Enter Employee Details </h3>
              <div class="col-md-10" style="margin-top:20px;">

              <div class="form-group">
                    <label class="control-label col-sm-4" for="fname">Select Company:</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="company_id" required>
                        @foreach($companies as $company)
                           <option  value="{{$company->id}}">{{$company->cname}}</option>
                        @endforeach   
                       </select>
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="fname">First Name:</label>
                    <div class="col-sm-8">
                       <input type="text" class="form-control" id="fname" placeholder="Enter First Name" required name="firstname" value="{{old('firstname')}}">
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="fname">Last Name:</label>
                    <div class="col-sm-8">
                       <input type="text" class="form-control" id="lname" placeholder="Enter Last Name" required name="lastname" value="{{old('lastname')}}">
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Email:</label>
                    <div class="col-sm-8">
                       <input type="email" class="form-control" id="email" placeholder="Email" required name="emp_email" value="{{old('emp_email')}}">
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="logo">Phone :</label>
                    <div class="col-sm-8">
                       <input type="number"  class="form-control"   required name="phone">
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="logo">Designation :</label>
                    <div class="col-sm-8">
                       <input type="text" class="form-control"   required name="designation">
                    </div>
                 </div>
              </div>
              <div class="col-md-10">
                 
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Status:</label>
                    <div class="col-sm-8">
                       <input type="radio" name="status" required value="active" checked>Active
                       <input type="radio" name="status" required value="inactive">Inactive
                    </div>
                 </div>
              </div>
            <div class="row">
               <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-8">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </div>
               @csrf
             </div>
           </form>
<!-- Tabs content -->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
         
         
        
         
         $(function () { //import-form
         $('#employee-form').parsley().on('field:validated', function() {
         var ok = $('.parsley-error').length === 0;
         $('.bs-callout-info').toggleClass('hidden', !ok);
         $('.bs-callout-warning').toggleClass('hidden', ok);
         })
         .on('form:submit', function() {
             return true;
         });
      });
      
      </script>
@endsection
