<div class="container" style="margin-top:30px;">
      @if ($errors->any())
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
   @if($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @elseif($message = Session::get('warning'))
   <div class="alert alert-warning alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
       <strong>{{ $message }}</strong>
   </div>
   @endif