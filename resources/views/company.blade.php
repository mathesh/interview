@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Company List
                <a href="{{url('company/create')}}" style="margin-left:60%;"> + Create Company</a></div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                  

<!-- Tabs content -->
        <table id="myTable" class="table table-striped table-bordered" style="width:100%">
            <thead>
               <tr>
                  <th>#S.No</th>
                  <th>Company Name</th>
                  <th>Email</th>
                  <th>Logo</th>
                  <th>Website</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
              <tbody id="data">
              @foreach($companies as $company)
            <tr>
                <td>#{{$company->id}}</td>
                <td>{{$company->cname}}</td>
                <td>{{$company->cemail}}</td>
                <td>{{$company->logo}}</td>
                <td>{{$company->website}}</td>
                <td>{{$company->status}}</td>
                <td> <a   style="cursor:pointer;" onclick="edit({{$company->id}});" data-toggle="modal">Edit</a>
                  / <a href="{{route('company.delete',$company->id)}}">Delete</a></td>
            </tr>
  @endforeach     

              </tbody>
         </table>
<!-- Tabs content -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Update Company</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form method="post" action="{{url('company/update')}}">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label col-sm-4" for="name">Name:</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control name" placeholder="Enter Fabric Type" required name="name">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-4" for="email">Email:</label>
                           <div class="col-sm-8">
                              <input type="email" class="form-control email" placeholder="Email" required name="email">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-4" for="mobile">Logo:</label>
                           <div class="col-sm-8 logo"  >
                              <input type="file" class="form-control mobile"  required name="mobile" id="logo" >
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-4" for="website">Website:</label>
                           <div class="col-sm-8">
                              <textarea type="text" class="form-control website" placeholder="Enter Website" required name="website"></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-4" for="pincode">Status:</label>
                           <div class="col-sm-8">
                              <input type="radio" name="status" required value="active">Active
                              <input type="radio" name="status" required value="inactive">Inactive
                           </div>
                        </div>
                     </div>
               </div>
               <div class="modal-footer">
               <input type="hidden" class="id" name="id">
               {{csrf_field()}}
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-primary">Save changes</button>
               </div>
            </div>
         </div>
         </form>
      </div>
@endsection
