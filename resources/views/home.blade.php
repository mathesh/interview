@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

            <!-- Tabs navs -->
            <a href="{{url('company')}}">Company</a> / <a href="{{url('employee')}}">Employee</a>
            <!-- Tabs content -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
