@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Upload Company
                <a href="{{url('company/create')}}" style="margin-left:60%;"> + Create Company</a>/
                <a href="{{url('company')}}"> Company List</a></div>
                

<!-- Tabs content -->
        <h3 style="text-align:center";>Upload CSV file</h3>
         <div class="col-md-6">
         <div class="form-group">
         <form action="{{ route('company.upload') }}" method="POST" id="import-form" name="importform" enctype="multipart/form-data">
         @csrf
         <input type="file" name="import_file" class="form-control" required accept=".csv">
         <button type="submit" class="btn btn-success" style="margin-top:20px;">Import File</button>
         </form>
         </div>
         <div class="col-md-6">
          <h4>Sample File : </h4>Sample file  <a href="{{asset('company.csv')}}">Click here</a>
         </div>
         </div>
         <hr>

            
<!-- Tabs content -->

                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(function () { //import-form
$('#import-form').parsley().on('field:validated', function() {
         var ok = $('.parsley-error').length === 0;
         $('.bs-callout-info').toggleClass('hidden', !ok);
         $('.bs-callout-warning').toggleClass('hidden', ok);
         })
         .on('form:submit', function() {
           return true;
         });
        });        
</script>
@endsection
