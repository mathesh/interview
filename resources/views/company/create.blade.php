@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Company
                <a href="{{url('company')}}" style="margin-left:60%;"> Company List</a></div>
                <div class="card-body">
        <form class="form-horizontal" method="post" action="{{url('company')}}" id="company-form" enctype="multipart/form-data">
            <h3 style="text-align:center;">Enter Company Details </h3>
              <div class="col-md-10" style="margin-top:20px;">
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="name">Name:</label>
                    <div class="col-sm-8">
                       <input type="text" class="form-control" id="name" placeholder="Enter Name" required name="cname" value="{{old('cname')}}">
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Email:</label>
                    <div class="col-sm-8">
                       <input type="email" class="form-control" id="email" placeholder="Email" required name="cemail" value="{{old('cemail')}}">
                    </div>
                 </div>
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="logo">Logo:</label>
                    <div class="col-sm-8">
                       <input type="file"  id="logo"  required name="logo" accept="image/png, image/gif, image/jpeg">
                    </div>
                 </div>
              </div>
              <div class="col-md-10">
                 <div class="form-group">
                    <label class="control-label col-sm-4" for="website">Website:</label>
                    <div class="col-sm-8">
                       <input type="url" class="form-control" id="website" placeholder="Enter Website" required name="website" value="{{old('website')}}">
                    </div>
                 </div>

                 <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Status:</label>
                    <div class="col-sm-8">
                       <input type="radio" name="status" required value="active" checked>Active
                       <input type="radio" name="status" required value="inactive">Inactive
                    </div>
                 </div>
              </div>
            <div class="row">
               <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-8">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </div>
               @csrf
             </div>
           </form>
<!-- Tabs content -->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
         
         
         $(document).ready(function()
         {   
      //   loadData();
         });
         
         function loadData()
         {   
         $.ajax({
             type:"get",
             url:"{{url('company')}}",
             success:function(res){
                 $('#data').html(res);
             }
         });
         return false;
         }
         
         $(function () { //import-form
         $('#company-form').parsley().on('field:validated', function() {
         var ok = $('.parsley-error').length === 0;
         $('.bs-callout-info').toggleClass('hidden', !ok);
         $('.bs-callout-warning').toggleClass('hidden', ok);
         })
         .on('form:submit', function() {
             return true;
         });

        //  $('#import-form').parsley().on('field:validated', function() {
        //  var ok = $('.parsley-error').length === 0;
        //  $('.bs-callout-info').toggleClass('hidden', !ok);
        //  $('.bs-callout-warning').toggleClass('hidden', ok);
        //  })
        //  .on('form:submit', function() {
        //    return true;
        //  });
         });
      </script>
@endsection
