@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Company List
                <a href="{{url('company/create')}}" style="margin-left:60%;"> + Create Company</a>/
                <a href="{{route('company.import')}}"> Bulk upload</a></div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                  

<!-- Tabs content -->
        <table id="myTable" class="table table-striped table-bordered" style="width:100%">
            <thead>
               <tr>
                  <th>#S.No</th>
                  <th>Company Name</th>
                  <th>Email</th>
                  <th>Logo</th>
                  <th>Website</th>
                  <th>Total Employee</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
              <tbody id="data">
              @foreach($companies as $company)
            <tr>
                <td>#{{$company->id}}</td>
                <td>{{$company->cname}}</td>
                <td>{{$company->cemail}}</td>
                <td><img src="{{asset('logo/'.$company->logo)}}" style="width:100px;height:100px;" ></td>
                <td>{{$company->website}}</td>
                <td>{{$company->employee_count}}</td>
                <td>{{ucfirst($company->status)}}</td>
                <td> <a  href="{{route('company.edit', $company->uuid)}}">Edit</a>
                  / <a href="{{url('company/delete',$company->uuid)}}">Delete</a></td>
            </tr>
  @endforeach     

              </tbody>
         </table>
<!-- Tabs content -->
{{ $companies->links() }}

                </div>
            </div>
        </div>
    </div>
</div>
<script>

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#logo').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this);
});


function edit(id) {
           $.ajax({
             type : 'get',
             url  : url,
             data : {'id':id},
             success:function(data){
               console.log(data);
               $('.id').val(data.id);
               $('.name').val(data.cname);
               $('.email').val(data.cemail);
               $('.website').val(data.website);
               if(data.status == 'active')
                   $("#active").prop("checked", true);
               else
                   $("#inactive").prop("checked", true);
               $('#logo').attr('src',data.logo);
               $('#exampleModal').modal();
             }
           });
         }

         $(function () { //import-form
         $('#edit-form').parsley().on('field:validated', function() {
         var ok = $('.parsley-error').length === 0;
         $('.bs-callout-info').toggleClass('hidden', !ok);
         $('.bs-callout-warning').toggleClass('hidden', ok);
         })
         .on('form:submit', function() {
             return true;
         });     
         });
</script>
@endsection
