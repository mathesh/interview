@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Company
                <a href="{{url('company')}}" style="margin-left:60%;"> Company List</a></div>
                <div class="card-body">
        <form class="form-horizontal" method="post" action="{{url('company/update')}}" id="company-form" enctype="multipart/form-data">
            <h3 style="text-align:center;">Enter Company Details </h3>
            <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label col-sm-4" for="name">Name:</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control name" placeholder="Enter Name" required name="cname" value="{{old('cname',$company->cname)}}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-4" for="email">Email:</label>
                           <div class="col-sm-8">
                              <input type="email" class="form-control email" placeholder="Email" required name="cemail" value="{{old('cname',$company->cemail)}}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-4" for="logo">Logo:</label>
                           <div class="col-sm-8">
                              <input type="file" class="form-control logo"  name="logo" id="imgInp" >
                              <img src="{{$company->logo}}" id="logo" style="width:100px;height:100px;" />
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-4" for="website">Website:</label>
                           <div class="col-sm-8">
                              <input type="url" class="form-control website" placeholder="Enter Website" required name="website"  value="{{old('website',$company->website)}}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-4" for="pincode">Status:</label>
                           <div class="col-sm-8">
                              <input type="radio" name="status" id="active" @if($company->status=='active') checked @endif value="active">Active
                              <input type="radio" name="status"  id="inactive"  @if($company->status=='inactive') checked @endif value="inactive">Inactive
                           </div>
                        </div>
                     </div>
                  <div class="row">
                     <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                           @csrf
                           <input type="hidden" name="id" value="{{$company->uuid}}">
                           <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                     </div>
                  </div>
           </form>
<!-- Tabs content -->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
         
         function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#logo').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this);
});
        
         
         $(function () { //import-form
         $('#company-form').parsley().on('field:validated', function() {
         var ok = $('.parsley-error').length === 0;
         $('.bs-callout-info').toggleClass('hidden', !ok);
         $('.bs-callout-warning').toggleClass('hidden', ok);
         })
         .on('form:submit', function() {
             return true;
         });
       
         });
      </script>
@endsection
