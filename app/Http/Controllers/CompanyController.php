<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Company;
use Illuminate\Support\Facades\Crypt;
use Validator;
use Webpatser\Uuid\Uuid;


class CompanyController extends Controller
{
    protected $paginate =10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::orderBy('id','desc')->withCount('employee')->paginate($this->paginate);
        return view('company.list',compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //  dd(request()->all());
        $validator = Validator::make(request()->all(), Company::$rules);
        if ($validator->fails()) {
            return back()->withErrors($validator->errors())->withInput();
        }
        $imageName = time().'.'.request('logo')->extension();  
        $data =  request()->except('_token');
        $data['logo'] = $imageName;
        $data['uuid'] = Uuid::generate();
        $company = Company::create($data); 
        if($company)
        {
            request('logo')->move(public_path('logo'), $imageName);
            return redirect()->to('company')->with('success', 'Company created successfully.');
        } 
        else
        {
            return redirect()->back()->with('warning', 'Company not created.');    
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::where('uuid',$id)->first();
        $company->logo = env('ASSETS_URL').$company->logo;
        return view('company.edit',compact('company'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = request('id');
        $company = Company::where('uuid',$id)->first();
        if(!$company)
           return redirect()->back()->with('warning', 'Company details not found.');       

        $validator = Validator::make(request()->all(), ['cname'=>'required|min:2|max:20',
        'cemail'=>'required|email|unique:companies,cemail,'.$company->id,'logo'=>'image|mimes:jpeg,png,jpg|max:2048',
        'website'=>'required','status'=>'required']);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors())->withInput();
        }

        $data =  request()->except(['_token','id']);
        if(request('logo')){
            $imageName = time().'.'.request('logo')->extension();  
            $data['logo'] = $imageName;
            request('logo')->move(public_path('logo'), $imageName);
        }
        $update = $company->fill($data)->save();
        if($update)
            return redirect()->to('company')->with('success', 'Company details updated successfully.');
        else
            return redirect()->back()->with('warning', 'Company details not updated.');       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::where('uuid',$id)->delete();
        if(!$company)
            return redirect()->back()->with('warning', 'Company details not found.'); 
        return redirect()->back()->with('success', 'Company deleted successfully.');
    }

}   
