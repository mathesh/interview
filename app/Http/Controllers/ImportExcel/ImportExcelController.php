<?php
 
namespace App\Http\Controllers\ImportExcel;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use App\Company;
use App\Jobs\TestJob;
use App\Jobs\EmployeeImport;


class ImportExcelController extends Controller
 
{
 
    public function indexCompany()
    {
        return view('company.import');
    }
 
    public function importCompany(Request $request)
    {
        $file = request()->file('import_file');
        $validator = Validator::make(
            [
                'file'      => $file,
                'extension' => strtolower($file->getClientOriginalExtension()),
            ],
            [
                'file'          => 'required',
                'extension'      => 'required|in:csv',
            ]
          );
        
        if ($validator->fails()) {
            return back()->withErrors($validator->errors())->withInput();
        }
        $name = 'company_'.time().'.'.$file->extension();
        $file->move(public_path('csv'), $name);
        TestJob::dispatch($name);
        return back()->with('success', 'Company imported successfully.');
    }

    public function indexEmployee()
    {
        return view('employee.import');
    }

    public function importEmployee(Request $request)
    {
        $file = request()->file('import_file');
        $validator = Validator::make(
            [
                'file'      => $file,
                'extension' => strtolower($file->getClientOriginalExtension()),
            ],
            [
                'file'          => 'required',
                'extension'      => 'required|in:csv',
            ]
          );
        
        if ($validator->fails()) {
            return back()->withErrors($validator->errors())->withInput();
        }
        $name = 'employee_'.time().'.'.$file->extension();
        $file->move(public_path('csv'), $name);
        EmployeeImport::dispatch($name);
        return back()->with('success', 'Employee imported successfully.');
    }

}