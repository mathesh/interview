<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Employee;
use Illuminate\Support\Facades\Crypt;
use Validator;
use Webpatser\Uuid\Uuid;
use App\Company;


class EmployeeController extends Controller
{
    protected $paginate =10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::orderBy('id','desc')->paginate($this->paginate);
        return view('employee.list',compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::where('status','active')->get();
        return view('employee.create',compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //  dd(request()->all());
        $validator = Validator::make(request()->all(), Employee::$rules);
        if ($validator->fails()) {
            return back()->withErrors($validator->errors())->withInput();
        }
        $data =  request()->except('_token');
        $data['uuid'] = Uuid::generate();
        $Employee = Employee::create($data); 
        if($Employee)
        {
            return redirect()->to('employee')->with('success', 'Employee created successfully.');
        } 
        else
        {
            return redirect()->back()->with('warning', 'Employee not created.');    
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::where('uuid',$id)->first();
        $companies = Company::where('status','active')->get();
        return view('employee.edit',compact('employee','companies'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = request('id');
        $employee = Employee::where('uuid',$id)->first();
        if(!$employee)
           return redirect()->back()->with('warning', 'Employee details not found.');       

        $validator = Validator::make(request()->all(), [ 'firstname'=>'required|min:2|max:20',
        'lastname'=>'required|min:2|max:20',
        'emp_email'=>'required|email|unique:employees,emp_email,'.$employee->id,
        'phone'=>'required|max:12|unique:employees,phone,'.$employee->id,
        'designation'=>'required',
        'status'=>'required','company_id'=>'required']
    );

        if ($validator->fails()) {
            return back()->withErrors($validator->errors())->withInput();
        }

        $data =  request()->except(['_token','id']);
        $update = $employee->fill($data)->save();
        if($update)
            return redirect()->to('employee')->with('success', 'Employee details updated successfully.');
        else
            return redirect()->back()->with('warning', 'Employee details not updated.');       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::where('uuid',$id)->delete();
        if(!$employee)
            return redirect()->back()->with('warning', 'Employee details not found.');     
        return redirect()->back()->with('success', 'Employee deleted successfully.');
    }

}   
