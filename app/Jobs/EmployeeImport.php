<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Webpatser\Uuid\Uuid;
use App\Employee;


class EmployeeImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private  $file;
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $location = 'csv';
  
        // Upload file

        $filepath =  public_path($location."/".$this->file);
        // Reading file
        $file = fopen($filepath,"r");

        $importData_arr = array();
        $i = 0;

        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
        $num = count($filedata );
        
        // Skip first row (Remove below comment if you want to skip the first row)
        /*if($i == 0){
            $i++;
            continue; 
        }*/
        for ($c=0; $c < $num; $c++) {
            $importData_arr[$i][] = $filedata [$c];
        }
        $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach($importData_arr as $importData){
        $insertData = array(
            "uuid"=>Uuid::generate(),
            "firstname"=>$importData[0],
            "lastname"=>$importData[1],
            "emp_email"=>$importData[2],
            "phone"=>$importData[3],
            "designation"=>$importData[4],
            "status"=>$importData[5],
            "company_id"=>$importData[6],
        );
        echo $importData[6];
        Employee::create($insertData);
         }
    }
}
