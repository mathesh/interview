<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Imports\ImportUsers;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Company;
use Webpatser\Uuid\Uuid;

class TestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private  $file;
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
             // File upload location
             $location = 'csv';
  
             // Upload file
   
             $filepath =  public_path($location."/".$this->file);
             // Reading file
             $file = fopen($filepath,"r");
   
             $importData_arr = array();
             $i = 0;
   
             while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                $num = count($filedata );
                
                // Skip first row (Remove below comment if you want to skip the first row)
                /*if($i == 0){
                   $i++;
                   continue; 
                }*/
                for ($c=0; $c < $num; $c++) {
                   $importData_arr[$i][] = $filedata [$c];
                }
                $i++;
             }
             fclose($file);
   
             // Insert to MySQL database
             foreach($importData_arr as $importData){
               $insertData = array(
                  "uuid"=>Uuid::generate(),
                  "cname"=>$importData[0],
                  "cemail"=>$importData[1],
                  "logo"=>$importData[2],
                  "website"=>$importData[3],
                  "status"=>$importData[4]);
               Company::create($insertData);
   
         }
    }
}
