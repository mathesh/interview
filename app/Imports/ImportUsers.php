<?php

namespace App\Imports;

use App\Company;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportUsers implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Company([
            'category'     => @$row['category'],
            'name'    => @$row['title'],
            'code'   => @$row['coupon_code'],
            'price'  => @$row['price'],
            'quantity'  => @$row['quantity'],
            'size'=>  @$row['size'],
            'color'  => @$row['color'],
            'long_description'=>  @$row['description'],
            'weight'  => @$row['weight'],
            'dimension'=>  @$row['dimensions'],
            'short_description'  => @$row['subtitle'],
            'discount_price'=>  @$row['discounted_price'],
            'images'=>  @$row['image_location'],
          //  'created_at'=>  @$row['images'],
            //'updated_at'=>  @$row['images'],

        ]);
    }
}
