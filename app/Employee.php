<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['uuid','firstname','lastname','emp_email','phone','designation','status','company_id'];

    public static $rules = array(
        'firstname'=>'required|min:2|max:20',
        'lastname'=>'required|min:2|max:20',
        'emp_email'=>'required|email|unique:employees,emp_email',
        'phone'=>'required|unique:employees,phone|max:12',
        'designation'=>'required',
        'status'=>'required','company_id'=>'required'
    );


}
