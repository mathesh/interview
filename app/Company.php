<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;

class Company extends Model
{
    protected $fillable = ['uuid','cname','cemail','logo','website','status'];

    public static $rules = array(
        'cname'=>'required|min:2|max:20',
        'cemail'=>'required|email|unique:companies',
        'logo'=>'image|mimes:jpeg,png,jpg|max:2048',
        'website'=>'required',
        'status'=>'required');

    public function employee()
    {
        return $this->hasMany(Employee::class);
    }

    
}
